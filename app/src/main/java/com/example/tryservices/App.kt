package com.example.tryservices

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build

class App: Application() {
    companion object {
        const val NOTIFICATION_CHANNEL_ID = "9cdf652e-70ee-4da2-a8fc-aead92fc8a3d"
    }

    override fun onCreate() {
        super.onCreate()

        createNotificationChanel()
    }

    private fun createNotificationChanel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel: NotificationChannel = NotificationChannel(
                NOTIFICATION_CHANNEL_ID,
                "Example service channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )

            val notificationManager: NotificationManager = getSystemService(NotificationManager::class.java)
            notificationManager.createNotificationChannel(notificationChannel)
        }
    }
}