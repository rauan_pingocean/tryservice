package com.example.tryservices

import android.Manifest
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*
import pub.devrel.easypermissions.AfterPermissionGranted
import kotlin.math.log
import pub.devrel.easypermissions.EasyPermissions
import android.content.pm.PackageManager
import android.os.IBinder
import android.widget.Toast






class MainActivity : AppCompatActivity() {

    var mService: ExampleService? = null
    var isBound: Boolean = false

    private val mServiceConnection: ServiceConnection = object: ServiceConnection {
        override fun onServiceConnected(clasName: ComponentName?, service: IBinder?) {
            var binder = service as ExampleService.IBinder
            mService = binder.getService()
            isBound = true

            logMain(message = "onServiceConnected")
        }

        override fun onServiceDisconnected(clasName: ComponentName?) {
            isBound = false

            logMain(message = "onServiceDisconnected")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        askPermission()

        btnStartService.setOnClickListener {
            val input = etInput.text.toString()

            val intentStartExampleService = Intent(this, ExampleService::class.java)
            intentStartExampleService.putExtra(ExampleService.EXTRA_INPUT_STRING, input)
//            startService(intentStartExampleService) // Service stop working on android 8, 7 when i'am calling

            bindService(intentStartExampleService, mServiceConnection, Context.BIND_AUTO_CREATE)
            ContextCompat.startForegroundService(this, intentStartExampleService)
            logMain(message = "btn: Start Service")
        }

        btnStopService.setOnClickListener {
            val intentStopExampleService = Intent(this, ExampleService::class.java)
            stopService(intentStopExampleService)

            logMain(message = "btn: Stop Service")
        }


        btnCallFirst.setOnClickListener {
            val callIntent: Intent = Intent(Intent.ACTION_DIAL)
            callIntent.data = Uri.parse("tel:+77021730578")
            startActivity(callIntent)
        }

        btnCallSecond.setOnClickListener {
            val callIntent: Intent = Intent(Intent.ACTION_DIAL)
            callIntent.data = Uri.parse("tel:+77787020514")
            startActivity(callIntent)
        }

        logMain(message = "onCreate")

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_DENIED || checkSelfPermission(
                    Manifest.permission.CALL_PHONE
                ) == PackageManager.PERMISSION_DENIED
            ) {
                val permissions = arrayOf(Manifest.permission.READ_PHONE_STATE, Manifest.permission.CALL_PHONE)
                requestPermissions(permissions, READ_PHONE_STATE_AND_CALL_PHONE)
            }
        }
    }

    override fun onStart() {
        super.onStart()

        logMain(message = "onStart")
    }

    override fun onResume() {
        super.onResume()

        logMain(message = "onResume")
    }

    override fun onPause() {
        super.onPause()

        logMain(message = "onPause")
    }

    override fun onRestart() {
        super.onRestart()

        logMain(message = "onRestart")
    }

    override fun onStop() {
        super.onStop()

        logMain(message = "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()

        unbindService(mServiceConnection)
        logMain(message = "onDestroy")
    }

    @AfterPermissionGranted(READ_PHONE_STATE_AND_CALL_PHONE)
    fun askPermission() {
        val perms = arrayOf(Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION)
        logMain(message = "askPermission: $perms")

        if (EasyPermissions.hasPermissions(this, *perms)) {
            // Already have permission, do the thing
            logMain(message = "Already have permission, do the thing")
            // ...
        } else {
            // Do not have permissions, request them now
            logMain(message = "Do not have permissions, request them now")
            EasyPermissions.requestPermissions(
                this, getString(R.string.read_phone_state_and_call_phone),
                READ_PHONE_STATE_AND_CALL_PHONE, *perms
            )
        }
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            READ_PHONE_STATE_AND_CALL_PHONE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Permission granted: $READ_PHONE_STATE_AND_CALL_PHONE", Toast.LENGTH_SHORT)
                        .show()
                } else {
                    Toast.makeText(
                        this,
                        "Permission NOT granted: $READ_PHONE_STATE_AND_CALL_PHONE",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                return
            }
        }
        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    companion object {
        var simpleName = MainActivity::class.java.simpleName

        const val READ_PHONE_STATE_AND_CALL_PHONE = 1

        fun logMain(from: String = simpleName, message: String) {
            Log.i("main >>>> $from ", message)
        }
    }
}
