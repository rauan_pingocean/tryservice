package com.example.tryservices

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.TelephonyManager
import android.widget.Toast
import java.lang.reflect.Method

class IncomingCallReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {

        MainActivity.logMain("IncomingCallReceiver", "onReceive")

        val telephonyService: ITelephony
        try {
            val state = intent.getStringExtra(TelephonyManager.EXTRA_STATE)
            val number = intent.extras!!.getString(TelephonyManager.EXTRA_INCOMING_NUMBER)

            if (state!!.equals(TelephonyManager.EXTRA_STATE_RINGING, ignoreCase = true)) {
                val tm = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
                try {
                    val m = tm.javaClass.getDeclaredMethod("getITelephony")

                    m.isAccessible = true
                    telephonyService = m.invoke(tm) as ITelephony

                    if (number != null) {
                        telephonyService.endCall()
                        MainActivity.logMain("IncomingCallReceiver", "Ending the call from: $number")
                        Toast.makeText(context, "Ending the call from: $number", Toast.LENGTH_SHORT).show()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }

                MainActivity.logMain("IncomingCallReceiver", "Ring: $number")
                Toast.makeText(context, "Ring " + number!!, Toast.LENGTH_SHORT).show()

            }
            if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK, ignoreCase = true)) {
                MainActivity.logMain("IncomingCallReceiver", "Answered: $number")
                Toast.makeText(context, "Answered " + number!!, Toast.LENGTH_SHORT).show()
            }
            if (state.equals(TelephonyManager.EXTRA_STATE_IDLE, ignoreCase = true)) {
                MainActivity.logMain("IncomingCallReceiver", "Idle: $number")
                Toast.makeText(context, "Idle " + number!!, Toast.LENGTH_SHORT).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            MainActivity.logMain("IncomingCallReceiver", "error: ${e.message}")
        }

    }
}