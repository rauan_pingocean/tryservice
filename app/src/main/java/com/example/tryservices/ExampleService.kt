package com.example.tryservices

import android.app.Notification
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import androidx.core.app.NotificationCompat
import java.util.*

class ExampleService: Service() {
    private var iBinder = IBinder()
    private var timerCounter = 0
    private lateinit var timer: Timer

    private var timerTask: TimerTask = object: TimerTask() {
        override fun run() {

            MainActivity.logMain(simpleName, message = "run: $timerCounter")

            timerCounter++
        }
    }

    override fun onCreate() {

        MainActivity.logMain(simpleName, "onCreate")

        super.onCreate()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val input: String = intent!!.getStringExtra(EXTRA_INPUT_STRING)!!

        timer = Timer()
        timer.schedule(timerTask, 1000, 1000)

        // Foreground notification
        val notificationIntent: Intent = Intent(this, MainActivity::class.java)
        val notificationPendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0)

        val notification: Notification = NotificationCompat.Builder(this, App.NOTIFICATION_CHANNEL_ID)
            .setContentText("Example Service")
            .setContentText(input)
            .setSmallIcon(R.drawable.ic_android_black_24dp)
            .setContentIntent(notificationPendingIntent)
            .build()

         startForeground(1, notification)

        MainActivity.logMain(simpleName, "onStartCommand")

        return START_REDELIVER_INTENT
    }

    override fun onDestroy() {
        timer.cancel()
        stopForeground(false)

        MainActivity.logMain(simpleName, "onDestroy")

        super.onDestroy()
    }

    override fun onBind(intent: Intent?): IBinder? {
        return iBinder
    }

    inner class IBinder: Binder() {
        fun getService(): ExampleService = this@ExampleService
        fun getCounter(): Int = timerCounter
    }

    companion object {
        var simpleName: String = ExampleService::class.java.simpleName

        // Extras
        const val EXTRA_INPUT_STRING = "3c10b361-857c-4fd6-ac52-719c082d0bf0"
    }

}